import os.path
import os
import gzip
import pickle

mnist = ('mnist.pkl.gz', 'http://deeplearning.net/data/mnist/mnist.pkl.gz')

dataset_hash = {'mnist': mnist}

def wgetter(fname):
    print 'Downloading: ' + fname + ' ...'
    os.system('wget ' + fname)

def get_dataset(dsname):
    (fname, url) = dataset_hash[dsname]
    if not os.path.isfile(fname):
        wgetter(url)
    with gzip.open(fname, 'r') as fd:
        return pickle.load(fd)
